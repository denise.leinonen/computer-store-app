let balance = 200
let balanceEL = document.getElementById("balance")
balanceEL.innerHTML = balance
let earned = 0
let earnedMoneyEL = document.getElementById("earnedMoney")
earnedMoneyEL.innerHTML = earned

let textInfo = document.getElementById("demo");
let loan = document.getElementById("payLoan");
let bankBtn = document.getElementById("bank");
let isLoaned =  false
let loaned

function workToIncreaseBalanceVolume(){
    let x = 100
    earned = earned + x 
     document.getElementById("earnedMoney").innerHTML = earned
}

function transferEarnedMoneyToBalance(){
    if(earned != 0){ //checks if you've earned something
        if(loaned > 10){ //If their is a loan you should pay 10% when you press "bank"
            let loanPercent = (10 / 100) * earned
            loaned -= loanPercent
            balance += earned - loanPercent
            earned = 0
            balanceEL.innerHTML = balance
            earnedMoneyEL.innerHTML = earned
        } else if(loaned <= 10){
            balance += earned - loaned
            earned = 0
            balanceEL.innerHTML = balance
            earnedMoneyEL.innerHTML = earned
            loaned = 0 
            loan.style.display = "none"
        } // transfers the money from salary to your account balance
        balance = balance + earned
        earned = 0
        balanceEL.innerHTML = balance
        earnedMoneyEL.innerHTML = earned
    }
}

function insertLoanValue(){
    let loanValue = prompt("How much will you loan? ", 0);

    if (loanValue == null || loanValue == "" || isNaN(loanValue)) {
        textInfo.innerHTML = "Something went wrong. check input type.";
    }else if(loaned > 0){
        textInfo.innerHTML = "Loan allready exists";
    }else if(loanValue > balance * 2){
        textInfo.innerHTML = "you can not loan that value";
    }else if( loanValue < 0){
        textInfo.innerHTML = "can't loan a negative number";
    } else {
      loaned = parseInt(loanValue)
      balance = balance + loaned
      document.getElementById("balance").innerHTML = balance;
      isLoaned = true
      loan.style.display = "inline"
    }
}
function payLoan(){
    if(loaned > 0 && earned > 0){
        let delta = loaned - earned
        if(delta <= 0){ // loaned paid of
            loan.style.display = "none" 
            balance = balance - loaned
            earned = -delta
            isLoaned = false
        }else{// still have loan left
            balance = balance - loaned
            loaned = -delta
        }
        balanceEL.innerHTML = balance
        earnedMoneyEL.innerHTML = earned
    }
}
function buyComputer(){
  if(computerPrices > balance){
    alert("You can't afford that computer")
  }else{
      balance = balance - computerPrices
      alert("You bought a " + computerTitle)
      balanceEL.innerHTML = balance
  }
}
