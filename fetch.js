
let computers = [];
let computerPrices = 0
let computerTitle = ""
const imagesElement = document.getElementById("computerImage")
const computersElement = document.getElementById("computers")
const descriptionsElement = document.getElementById("descriptionId")
const priceElement = document.getElementById("priceId")
const titleElement = document.getElementById("titleId")
const specsElement = document.getElementById("specsId")
const buyComputerElement = document.getElementById("buy")


fetch(" https://noroff-komputer-store-api.herokuapp.com/computers")
  .then(response => response.json())
  .then(data => computers = data)
  .then(computers => addComputersToMenu(computers) )

const addComputersToMenu = (computer) =>{
  computer.forEach(element => addComputerToMenu(element));
  priceElement.innerText = computers[0].price
  descriptionsElement.innerText = computers[0].description
  titleElement.innerText = computers[0].title
  specsElement.innerText = computers[0].specs
  const image = document.getElementById('computerImage');
  image.setAttribute(
    'src', 
    `https://noroff-komputer-store-api.herokuapp.com/${computers[0].image}`
  )
}

const addComputerToMenu = (computer) =>{
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  computersElement.appendChild(computerElement);
}

const handlePriceInfo = e =>{
  const selectedComputer = computers[e.target.selectedIndex];
  priceElement.innerText = selectedComputer.price
  computerPrices = selectedComputer.price
}

const handledescription = e =>{
  const selectedComputer = computers[e.target.selectedIndex];
  descriptionsElement.innerHTML = selectedComputer.description
}

const handletitle = e =>{
  const selectedComputer = computers[e.target.selectedIndex];
  titleElement.innerHTML = selectedComputer.title
  computerTitle = selectedComputer.title
}

const handleimage= e =>{
  const selectedComputer = computers[e.target.selectedIndex];
  const image = document.getElementById('computerImage');
  image.setAttribute(
    'src', 
    `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`
  )
}

const handlespecs= e =>{
  const selectedComputer = computers[e.target.selectedIndex];
  specsElement.innerHTML = selectedComputer.specs
}
computersElement.addEventListener("change", handlePriceInfo);
computersElement.addEventListener("change", handledescription);
computersElement.addEventListener("change", handletitle);
computersElement.addEventListener("change", handleimage);
computersElement.addEventListener("change", handlespecs);
